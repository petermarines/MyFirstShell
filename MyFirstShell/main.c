#include<stdio.h>
#include<stdin.h>

#define SHELL_RL_BUFSIZE 1024
char *shell_read_line(void){

    int bufsize = SHELL_RL_BUFSIZE;
    int position = 0;
    char * buffer = malloc(sizeof(char)* bufsize);
    int c;

    if(!buffer){
        fprintf(stderr, "shell: Memory allocation error.");
        exit(EXIT_FAILURE);
    }

    while(1){
        c = getchar();

        if(c == EOF || c == '\n'){
            buffer[position] = '\n';
            return buffer;
        } else {
            buffer[position] = c;
        }
        position++;

        if(position >= bufsize){
            bufsize += SHELL_RL_BUFSIZE;
            buffer = realloc(position, bufsize * sizeof(char));
            if(!buffer){
                fprintf(stderr, "shell: Memory allocation error.");
                exit(EXIT_FAILURE);
            }
        }
    }
}

#define SHELL_TOK_BUFSIZE 64
#define SHELL_TOK_DELIM " \t\r\n\a"
char ** shell_split_line(char * line){

    int bufsize = SHELL_TOK_BUFSIZE;
    int position = 0;
    char ** tokens = malloc(sizeof(char*) * bufsize);
    char * token;

    if(!tokens){
        fprintf(stderr, "shell: Memory allocation error.");
        exit(EXIT_FAILURE);
    }

    token = strtok(line, SHELL_TOK_DELIM);

    while(token != NULL){
        tokens[position] = token;
        position++;

        if(position >= bufsize){
            bufsize += SHELL_TOK_BUFSIZE;
            tokens = realloc(position, bufsize * sizeof(char*));
            if(!tokens){
                fprintf(stderr, "shell: Memory allocation error.");
                exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, SHELL_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}

int shell_execute(){

    return;
}

void shell_loop(void){
    char *line;
    char **args;
    int status;

    while(status){
        printf("> ");
        line = shell_read_line();
        args = shell_split_line();
        status = shell_execute(args);

        free(line);
        free(args);
    }

}

int main(int argc, char **argv){

    shell_loop();

    return EXIT_SUCCESS;
}
